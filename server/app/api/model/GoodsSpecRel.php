<?php

declare (strict_types = 1);

namespace app\api\model;

use app\common\model\GoodsSpecRel as GoodsSpecRelModel;

/**
 * 商品规格关系模型
 * Class GoodsSpecRel
 * @package app\api\model
 */
class GoodsSpecRel extends GoodsSpecRelModel
{
}
