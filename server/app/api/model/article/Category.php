<?php

declare (strict_types = 1);

namespace app\api\model\article;

use app\common\model\article\Category as CategoryModel;

/**
 * 文章分类模型
 * Class Category
 * @package app\api\model\article
 */
class Category extends CategoryModel
{
    /**
     * 获取分类列表(未隐藏的)
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getShowList()
    {
        $where = ['status', '=', 1];
        return $this->getList([$where]);
    }
}
