<?php

declare (strict_types = 1);

namespace app\api\model;

use app\common\model\GoodsSku as GoodsSkuModel;

/**
 * 商品规格模型
 * Class GoodsSku
 * @package app\api\model
 */
class GoodsSku extends GoodsSkuModel
{
    /**
     * 规格图片
     * @return \think\model\relation\HasOne
     */
    public function image()
    {
        return parent::image()->bind(['image_url' => 'preview_url']);
    }

    /**
     * 隐藏字段
     * @var array
     */
    protected $hidden = [
        'store_id',
        'create_time',
        'update_time'
    ];

}
