<?php









declare (strict_types = 1);

namespace app\api\model;

use app\common\model\UserOauth as UserOauthModel;

/**
 * 模型类：第三方用户信息
 * Class UserOauth
 * @package app\api\model
 */
class UserOauth extends UserOauthModel
{
    /**
     * 隐藏字段
     * @var array
     */
    protected $hidden = [
        'create_time',
        'update_time',
    ];

    /**
     * 新增数据
     * @param array $data
     * @return bool
     */
    public function add(array $data)
    {
        return $this->save($data);
    }
}
