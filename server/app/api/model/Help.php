<?php

declare (strict_types = 1);

namespace app\api\model;

use app\common\model\Help as HelpModel;

/**
 * 模型类：帮助中心
 * Class Help
 * @package app\api\model
 */
class Help extends HelpModel
{
    /**
     * 隐藏字段
     * @var array
     */
    protected $hidden = [
        'create_time',
        'update_time',
    ];
}
