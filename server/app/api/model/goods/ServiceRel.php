<?php

declare (strict_types = 1);

namespace app\api\model\goods;

use app\common\model\goods\ServiceRel as ServiceRelModel;

/**
 * 商品服务与承诺模型
 * Class ServiceRel
 */
class ServiceRel extends ServiceRelModel
{

}
