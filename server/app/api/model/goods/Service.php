<?php

declare (strict_types = 1);

namespace app\api\model\goods;

use app\common\model\goods\Service as ServiceModel;
use app\api\model\goods\ServiceRel as ServiceRelModel;

/**
 * 商品服务与承诺模型
 * Class Service
 */
class Service extends ServiceModel
{
    // 隐藏的字段
    protected $hidden = [
        'is_default',
        'status',
        'sort',
        'is_delete',
        'store_id',
        'update_time',
    ];

    /**
     * 获取指定商品的服务与承诺
     * @param int $goodsId
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getListByGoods(int $goodsId)
    {
        // 获取指定商品的服务承诺id集
        $serviceIds = ServiceRelModel::getServiceIds($goodsId);
        // 获取服务与承诺列表
        return $this->where('service_id', 'in', $serviceIds)
            ->where('status', '=', 1)
            ->where('is_delete', '=', 0)
            ->order(['sort', $this->getPk()])
            ->select();
    }

}
