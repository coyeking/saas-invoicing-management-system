<?php

declare (strict_types = 1);

namespace app\api\model;

use app\common\model\Region as RegionModel;

/**
 * 地区模型
 * Class Region
 * @package app\api\model
 */
class Region extends RegionModel
{

}
