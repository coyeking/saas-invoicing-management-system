<?php

declare (strict_types = 1);

namespace app\api\model;

use app\api\service\User as UserService;
use app\common\exception\BaseException;
use app\common\model\OrderGoods as OrderGoodsModel;

/**
 * 订单商品模型
 * Class OrderGoods
 * @package app\api\model
 */
class OrderGoods extends OrderGoodsModel
{
    /**
     * 隐藏字段
     * @var array
     */
    protected $hidden = [
        'content',
        'store_id',
        'create_time',
    ];

    /**
     * 获取未评价的商品
     * @param int $orderId 订单ID
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getNotCommentGoodsList(int $orderId)
    {
        return (new static)->with(['image'])
            ->where('order_id', '=', $orderId)
            ->where('is_comment', '=', 0)
            ->select();
    }

}
