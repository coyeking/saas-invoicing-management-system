<?php

declare (strict_types = 1);

namespace app\api\model;

use app\common\model\Store as StoreModel;

/**
 * 商家记录表模型
 * Class Store
 * @package app\store\model
 */
class Store extends StoreModel
{

}
