<?php

declare (strict_types = 1);

return [
    // 验证码图片宽度
    'imageW' => 0,
    // 验证码图片高度
    'imageH' => 0,
    // 验证码位数
    'length' => 4,
    // 是否画混淆曲线
    'useCurve' => false,
    // 是否添加杂点
    'useNoise' => true,
    // 验证码字体大小(px)
    'fontSize' => 26,
    // 验证码字符集合
    // 复杂版：2345678abcdefhijkmnpqrstuvwxyzABCDEFGHJKLMNPQRTUVWXY
    'codeSet' => '23456ACEFHJKLMNPRTUVWXY'
];