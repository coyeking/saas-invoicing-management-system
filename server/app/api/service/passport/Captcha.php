<?php









declare (strict_types = 1);

namespace app\api\service\passport;

use app\common\service\BaseService;
use edward\captcha\facade\CaptchaApi;

class Captcha extends BaseService
{
    /**
     * 图形验证码
     * @return array|\think\response\Json
     */
    public function create()
    {
        $data = CaptchaApi::create();
        return [
            'base64' => str_replace("\r\n", '', $data['base64']),
            'key' => $data['key'],
            'md5' => $data['md5']
        ];
    }
}