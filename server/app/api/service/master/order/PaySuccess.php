<?php









declare (strict_types = 1);

namespace app\api\service\master\order;

use app\common\service\BaseService;

/**
 * 普通订单支付成功后的回调
 * Class PaySuccess
 * @package app\api\service\master\order
 */
class PaySuccess extends BaseService
{
    /**
     * 回调方法
     * @param $order
     * @return bool
     */
    public function onPaySuccess($order)
    {
        // 暂无业务逻辑
        return true;
    }

}