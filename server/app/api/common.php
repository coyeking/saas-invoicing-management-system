<?php









declare (strict_types = 1);

// 应用公共函数库文件

/**
 * 获取当前访问的渠道(微信小程序、H5、APP等)
 * @return int|null
 */
function getPlatform()
{
    static $value = null;
    // 从header中获取 channel
    empty($value) && $value = request()->header('platform');
    // 调试模式下可通过param中获取
    if (is_debug() && empty($value)) {
        $value = request()->param('platform');
    }
    return $value;
}
