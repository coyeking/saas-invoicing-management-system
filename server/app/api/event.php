<?php









declare (strict_types = 1);

// 应用行为扩展定义文件
return [
    'listen' => [
        'OrderPaySuccess' => [
            \app\api\listener\order\PaySuccess::class
        ]
    ]
];
