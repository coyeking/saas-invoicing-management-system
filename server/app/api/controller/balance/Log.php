<?php

declare (strict_types = 1);

namespace app\api\controller\balance;

use app\api\controller\Controller;
use app\api\model\user\BalanceLog as BalanceLogModel;

/**
 * 余额账单明细
 * Class Log
 * @package app\api\controller\balance
 */
class Log extends Controller
{
    /**
     * 余额账单明细列表
     * @return array
     * @throws \app\common\exception\BaseException
     * @throws \think\db\exception\DbException
     */
    public function list()
    {
        $model = new BalanceLogModel;
        $list = $model->getList();
        return $this->renderSuccess(compact('list'));
    }

}