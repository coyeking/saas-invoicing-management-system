<?php

declare (strict_types = 1);

namespace app\api\controller\goods;

use app\api\controller\Controller;
use app\api\model\goods\Service as ServiceModel;

/**
 * 商品服务与承诺管理
 * Class Service
 * @package app\store\controller\goods
 */
class Service extends Controller
{
    /**
     * 获取指定商品的服务与承诺
     * @param int $goodsId
     * @return array|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function list(int $goodsId)
    {
        $model = new ServiceModel;
        $list = $model->getListByGoods($goodsId);
        return $this->renderSuccess(compact('list'));
    }

}
