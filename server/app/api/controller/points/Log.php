<?php

declare (strict_types = 1);

namespace app\api\controller\points;

use app\api\controller\Controller;
use app\api\model\user\PointsLog as PointsLogModel;

/**
 * 积分明细
 * Class Log
 * @package app\api\controller\balance
 */
class Log extends Controller
{
    /**
     * 积分明细列表
     * @return array
     * @throws \app\common\exception\BaseException
     * @throws \think\db\exception\DbException
     */
    public function list()
    {
        $model = new PointsLogModel;
        $list = $model->getList();
        return $this->renderSuccess(compact('list'));
    }
}