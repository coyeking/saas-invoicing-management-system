<?php

declare (strict_types = 1);

namespace app\api\controller\article;

use app\api\controller\Controller;
use app\api\model\article\Category as CategoryModel;

/**
 * 文章分类
 * Class Category
 * @package app\api\controller\article
 */
class Category extends Controller
{
    /**
     * 文章分类列表
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function list()
    {
        $model = new CategoryModel;
        $list = $model->getShowList();
        return $this->renderSuccess(compact('list'));
    }

}