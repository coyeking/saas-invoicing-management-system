<?php

declare (strict_types = 1);

namespace app\api\controller;

use app\api\model\Page as PageModel;

/**
 * 页面控制器
 * Class Index
 * @package app\api\controller
 */
class Page extends Controller
{
    /**
     * 页面数据
     * @param int|null $pageId 页面ID, 不传的话默认为首页
     * @return array|\think\response\Json
     * @throws \app\common\exception\BaseException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function detail(int $pageId = null)
    {
        // 页面数据
        $model = new PageModel;
        $pageData = $model->getPageData($pageId);
        return $this->renderSuccess(compact('pageData'));
    }
}
