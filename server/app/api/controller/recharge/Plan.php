<?php

declare (strict_types = 1);

namespace app\api\controller\recharge;

use app\api\controller\Controller;
use app\api\model\recharge\Plan as PlanModel;
use app\common\exception\BaseException;

/**
 * 充值套餐管理
 * Class Plan
 * @package app\api\controller\recharge
 */
class Plan extends Controller
{
    /**
     * 充值套餐列表
     * @return array
     * @throws BaseException
     * @throws \think\db\exception\DbException
     */
    public function list()
    {
        $model = new PlanModel;
        $list = $model->getList();
        return $this->renderSuccess(compact('list'));
    }

}