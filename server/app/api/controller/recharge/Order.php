<?php

declare (strict_types = 1);

namespace app\api\controller\recharge;

use app\api\controller\Controller;
use app\api\model\recharge\Order as OrderModel;
use app\common\exception\BaseException;

/**
 * 充值记录管理
 * Class Order
 * @package app\api\controller\recharge
 */
class Order extends Controller
{
    /**
     * 我的充值记录列表
     * @return array
     * @throws BaseException
     * @throws \think\db\exception\DbException
     */
    public function list()
    {
        $model = new OrderModel;
        $list = $model->getList();
        return $this->renderSuccess(compact('list'));
    }

}