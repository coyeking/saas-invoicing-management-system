<?php

declare (strict_types = 1);

namespace app\api\controller;

use app\api\model\Express as ExpressModel;

/**
 * 物流公司管理
 * Class Express
 * @package app\api\controller
 */
class Express extends Controller
{
    /**
     * 物流公司列表
     * @return array|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function list()
    {
        $model = new ExpressModel;
        $list = $model->getAll();
        return $this->renderSuccess(compact('list'));
    }
}