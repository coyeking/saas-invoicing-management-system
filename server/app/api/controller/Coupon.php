<?php

declare (strict_types = 1);

namespace app\api\controller;

use app\api\model\Coupon as CouponModel;

/**
 * 优惠券中心
 * Class Coupon
 * @package app\api\controller
 */
class Coupon extends Controller
{
    /**
     * 优惠券列表
     * @return array
     * @throws \app\common\exception\BaseException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function list()
    {
        $model = new CouponModel;
        $list = $model->getList();
        return $this->renderSuccess(compact('list'));
    }

}