<?php

declare (strict_types = 1);

namespace app\api\controller;

use app\api\model\Category as CategoryModel;

/**
 * 商品分类控制器
 * Class Category
 * @package app\api\controller
 */
class Category extends Controller
{
    /**
     * 分类列表
     * @return array|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function list()
    {
        $model = new CategoryModel;
        $list = $model->getList($this->request->param());
        return $this->renderSuccess(compact('list'));
    }

}
