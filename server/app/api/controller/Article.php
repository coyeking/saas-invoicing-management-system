<?php

declare (strict_types = 1);

namespace app\api\controller;

use app\api\model\Article as ArticleModel;

/**
 * 文章控制器
 * Class Article
 * @package app\api\controller
 */
class Article extends Controller
{
    /**
     * 文章列表
     * @param int $categoryId
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function list(int $categoryId = 0)
    {
        $model = new ArticleModel;
        $list = $model->getList($categoryId);
        return $this->renderSuccess(compact('list'));
    }

    /**
     * 文章详情
     * @param int $articleId
     * @return array|\think\response\Json
     * @throws \app\common\exception\BaseException
     */
    public function detail(int $articleId)
    {
        $detail = ArticleModel::getDetail($articleId);
        return $this->renderSuccess(compact('detail'));
    }
}
