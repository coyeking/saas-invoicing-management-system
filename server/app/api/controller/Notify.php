<?php

declare (strict_types = 1);

namespace app\api\controller;

use app\common\library\wechat\WxPay;
use app\common\exception\BaseException;

/**
 * 支付成功异步通知接口
 * Class Notify
 * @package app\api\controller
 */
class Notify
{
    /**
     * 支付成功异步通知(微信小程序-微信支付)
     * @throws BaseException
     */
    public function wxpay()
    {
        // 微信支付组件：验证异步通知
        $WxPay = new WxPay();
        return $WxPay->notify();
    }
}
