<?php

declare (strict_types = 1);

namespace app\api\controller;

use app\api\model\Help;

/**
 * 微信小程序
 * Class Wxapp
 * @package app\api\controller
 */
class Wxapp extends Controller
{
    /**
     * 小程序基础信息
     * @return array
     */
    public function base()
    {
        return $this->renderSuccess([]);
    }

    /**
     * 帮助中心
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function help()
    {
        $model = new Help;
        $list = $model->getList();
        return $this->renderSuccess(compact('list'));
    }

}
