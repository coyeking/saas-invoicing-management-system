<?php

declare (strict_types = 1);

namespace app\api\controller;

use app\api\model\Region as RegionModel;

/**
 * 地区管理
 * Class Region
 * @package app\api\controller
 */
class Region extends Controller
{
    /**
     * 获取所有地区
     * @return array|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function all()
    {
        $list = RegionModel::getCacheAll();
        return $this->renderSuccess(compact('list'));
    }

    /**
     * 获取所有地区(树状)
     * @return array|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function tree()
    {
        $list = RegionModel::getCacheTree();
        return $this->renderSuccess(compact('list'));
    }

}
