<?php









declare (strict_types = 1);

namespace app\api\validate\passport;

use think\Validate;

/**
 * 验证类：发送短信验证码
 * Class SmsCaptcha
 * @package app\api\validate\passport
 */
class SmsCaptcha extends Validate
{
    /**
     * 验证规则
     * @var array
     */
    protected $rule = [
        // 图形验证码 (用户输入)
        'captchaCode' => ['require'],
        // 图形验证码 (key)
        'captchaKey' => ['require'],
        // 用户手机号
        'mobile' => ['require'],
    ];

    /**
     * 验证提示
     * @var string[]
     */
    protected $message  =   [
        'captchaCode.require' => '图形验证码code不能为空',
        'captchaKey.require' => '图形验证码key不能为空',
        'mobile.require' => '手机号不能为空',
    ];
}