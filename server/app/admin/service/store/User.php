<?php

declare (strict_types = 1);

namespace app\admin\service\store;

use app\common\service\store\User as Basics;
use app\admin\model\store\User as StoreUserModel;

/**
 * 超管用户服务类
 * Class User
 */
class User extends Basics
{

}
