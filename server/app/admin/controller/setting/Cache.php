<?php

declare (strict_types = 1);

namespace app\admin\controller\setting;

use app\admin\controller\Controller;
use app\admin\service\Cache as CacheService;

/**
 * 清理缓存
 * Class Index
 * @package app\admin\controller
 */
class Cache extends Controller
{
    /**
     * 清理缓存
     * @return array|string
     * @throws \Exception
     */
    public function clear()
    {
        // 清理缓存
        $CacheService = new CacheService;
        if (!$CacheService->rmCache($this->postForm())) {
            return $this->renderError($CacheService->getError() ?: '操作失败');
        }
        return $this->renderSuccess('操作成功');
    }

}
