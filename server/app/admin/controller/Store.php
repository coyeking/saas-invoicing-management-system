<?php

declare (strict_types=1);

namespace app\admin\controller;

use app\admin\model\Store as StoreModel;

/**
 * 小程序商城管理
 * Class Store
 * @package app\admin\controller
 */
class Store extends Controller
{
    /**
     * 强制验证当前访问的控制器方法method
     * @var array
     */
    protected $methodRules = [
        'index' => 'GET',
        'recycle' => 'GET',
        'add' => 'POST',
        'move' => 'POST',
        'delete' => 'POST',
    ];

    /**
     * 小程序商城列表
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function index()
    {
        // 商城列表
        $model = new StoreModel;
        $list = $model->getList();
        return $this->renderSuccess(compact('list'));
    }

    /**
     * 回收站列表
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function recycle()
    {
        // 商城列表
        $model = new StoreModel;
        $list = $model->getList(true);
        return $this->renderSuccess(compact('list'));
    }

    /**
     * 回收小程序
     * @param int $storeId
     * @return array
     */
    public function recovery(int $storeId)
    {
        // 小程序详情
        $model = StoreModel::detail($storeId);
        if (!$model->recycle()) {
            return $this->renderError($model->getError() ?: '操作失败');
        }
        return $this->renderSuccess('操作成功');
    }

    /**
     * 移出回收站
     * @param int $storeId
     * @return array
     */
    public function move(int $storeId)
    {
        // 小程序详情
        $model = StoreModel::detail($storeId);
        if (!$model->recycle(false)) {
            return $this->renderError($model->getError() ?: '操作失败');
        }
        return $this->renderSuccess('操作成功');
    }

    public function add()
    {
        
        $model = new StoreModel;
        // 新增记录
        if ($model->add($this->postData('form'))) {
            return $this->renderError('添加失败1');
        }
        return $this->renderError($model->getError() ?: '添加失败');



        return $this->renderError('很抱歉，免费版暂不支持多开商城');
    }

}
