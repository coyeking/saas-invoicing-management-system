<?php

declare (strict_types = 1);

namespace app\admin\model;

use app\common\model\UploadFile as UploadFileModel;

/**
 * 文件库模型
 * Class UploadFile
 * @package app\admin\model
 */
class UploadFile extends UploadFileModel
{

}
