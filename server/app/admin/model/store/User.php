<?php

declare (strict_types = 1);

namespace app\admin\model\store;

use app\common\exception\BaseException;
use app\common\model\store\User as StoreUserModel;

/**
 * 商家用户模型
 * Class StoreUser
 * @package app\admin\model
 */
class User extends StoreUserModel
{
    /**
     * 商家用户登录
     * @param int $storeId
     * @throws BaseException
     * @throws \think\Exception
     */
    public function login(int $storeId)
    {
        // 获取该商户管理员用户信息
        $userInfo = $this->getSuperStoreUser($storeId);
        if (empty($userInfo)) {
            throwError('超级管理员用户信息不存在');
        }
    }

    /**
     * 获取该商户管理员用户信息
     * @param int $storeId
     * @return static|null
     */
    private function getSuperStoreUser(int $storeId)
    {
        return static::detail(['store_id' => $storeId, 'is_super' => 1], ['wxapp']);
    }

    /**
     * 删除小程序下的商家用户
     * @param $storeId
     * @return false|int
     */
    public static function setDelete(int $storeId)
    {
        static::update(['is_delete' => '1'], ['store_id' => $storeId]);
        return true;
    }
    
    /**
     * 新增商家用户记录
     * @param $store_id
     * @param $data
     * @return bool|false|int
     */
    public function add($store_id, $data)
    {
        return $this->save([
            'user_name' => $data['user_name'],
            'password' => encryption_hash($data['password']),
            'real_name' => '系统管理员',
            'store_id' => $store_id,
        ]);
    }

}
