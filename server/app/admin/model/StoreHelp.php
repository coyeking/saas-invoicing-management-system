<?php

namespace app\admin\model;

use app\common\model\Help as StoreHelpModel;

/**
 * 小程序帮助中心
 * Class StoreHelp
 * @package app\admin\model
 */
class StoreHelp extends StoreHelpModel
{
    /**
     * 新增默认帮助
     * @param $store_id
     * @return false|int
     */
    public function insertDefault($store_id)
    {
        return $this->save([
            'title' => '关于应用',
            'content' => '基于uniapp多端兼容发布，大大节约开发成本，跨平台使用，响应迅速',
            'sort' => 100,
            'store_id' => $store_id
        ]);
    }

}
