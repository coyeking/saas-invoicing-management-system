<?php

namespace app\admin\model;

use app\common\model\Wxapp as WxappModel;

/**
 * 微信小程序diy页面模型
 * Class Wxapp
 * @package app\admin\model
 */
class Wxapp extends WxappModel
{
    /**
     * 新增小程序首页diy默认设置
     * @param $store_id
     * @return false|int
     */
    public function insertDefault($store_id)
    {
        return $this->save([
            'store_id' => $store_id,
            'crecreate_timet' => time(),
            'update_time' => time(),
        ]);
    }

}
