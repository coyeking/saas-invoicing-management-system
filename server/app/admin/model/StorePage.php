<?php

namespace app\admin\model;

use app\common\model\Page as StorePageModel;

/**
 * 微信小程序diy页面模型
 * Class StorePage
 * @package app\admin\model
 */
class StorePage extends StorePageModel
{
    /**
     * 新增小程序首页diy默认设置
     * @param $store_id
     * @return false|int
     */
    public function insertDefault($store_id)
    {
        return $this->save([
            'page_type' => 10,
            'page_name' => '首页',
            'page_data' => [
                'page' => [
                    'name' => '页面设置',
                    'type' => 'page',
                    'params' => [
                        'name' => '页面名称',
                        'title' => '页面标题',
                        'shareTitle' => '分享标题'
                    ],
                    'style' => [
                        'titleTextColor' => 'black',
                        'titleBackgroundColor' => '#ffffff',
                    ]
                ],
                'items' => [
                    [
                        'name' => '图片',
                        'type' => 'image',
                        'style' => [
                            'paddingTop' => 0,
                            'paddingLeft' => 0,
                            'borderradius' => 0,
                            'background' => '#ffffff'
                        ],
                        'data' => [
                            [
                                'imgUrl' => base_url() . 'assets/store/img/diy/banner/01.png',
                                'imgName' => 'image-1.jpg',
                                'link' => null
                            ]
                        ]
                    ]
                ],
            ],
            'store_id' => $store_id
        ]);
    }

}
