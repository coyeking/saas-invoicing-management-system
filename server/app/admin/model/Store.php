<?php

declare (strict_types=1);

namespace app\admin\model;

use app\common\model\Store as StoreModel;
use app\admin\model\store\User as StoreUser;

/**
 * 商家记录表模型
 * Class Store
 * @package app\admin\model
 */
class Store extends StoreModel
{
    /**
     * 获取列表数据
     * @param bool $isRecycle
     * @return \think\Paginator
     * @throws \think\db\exception\DbException
     */
    public function getList(bool $isRecycle = false)
    {
        return $this->where('is_recycle', '=', (int)$isRecycle)
            ->where('is_delete', '=', 0)
            ->order(['create_time' => 'desc'])
            ->paginate(15);
    }

    /**
     * 移入移出回收站
     * @param bool $isRecycle
     * @return false|int
     */
    public function recycle($isRecycle = true)
    {
        return $this->save(['is_recycle' => (int)$isRecycle]);
    }

    
    /**
     * 新增记录
     * @param $data
     * @return bool|mixed
     */
    public function add($data)
    {
        if (StoreUser::checkExist($data['user_name'])) {
            $this->error = '商家用户名已存在';
            return false;
        }
        return $this->transaction(function () use ($data) {
            // 添加小程序记录感谢使用z7系统， https://www.z7sucai.cn
            $this->save([
                'store_name' => $data['store_name'],
                'describe' => '',
                'sort' => $data['sort'],
                'create_time' => time(),
                'update_time' => time(),
            ]);
            // 新增商家用户信息
            (new StoreUser)->add($this['store_id'], $data);
            // // 新增小程序默认帮助
            (new StoreHelp)->insertDefault($this['store_id']);
            // // 新增小程序diy配置
            (new StorePage)->insertDefault($this['store_id']);
            // // 新增配置
            (new Wxapp)->insertDefault($this['store_id']);
            // // 新增小程序分类页模板
            // (new WxappCategory)->insertDefault($this['wxapp_id']);
            return true;
        });
    }

}
