<?php
declare (strict_types = 1);

namespace app\store\controller\user;

use app\store\controller\Controller;
use app\store\model\recharge\Order as OrderModel;

/**
 * 余额记录
 * Class Recharge
 * @package app\store\controller\user
 */
class Recharge extends Controller
{
    /**
     * 充值记录
     * @return mixed
     */
    public function order()
    {
        $model = new OrderModel;
        $list = $model->getList($this->request->param());
        return $this->renderSuccess(compact('list'));
    }

}
