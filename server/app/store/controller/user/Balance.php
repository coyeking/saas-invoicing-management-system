<?php









declare (strict_types = 1);

namespace app\store\controller\user;

use app\store\controller\Controller;
use app\store\model\user\BalanceLog as BalanceLogModel;

/**
 * 余额明细
 * Class Balance
 * @package app\store\controller\user
 */
class Balance extends Controller
{
    /**
     * 余额明细
     * @return mixed
     */
    public function log()
    {
        $model = new BalanceLogModel;
        $list = $model->getList($this->request->param());
        return $this->renderSuccess(compact('list'));
    }

}
