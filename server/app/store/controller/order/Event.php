<?php









declare (strict_types = 1);

namespace app\store\controller\order;

use app\store\controller\Controller;
use app\store\model\Order as OrderModel;

/**
 * 订单操作控制器
 * Class Operate
 * @package app\store\controller\order
 */
class Event extends Controller
{
    // 订单模型类
    /* @var OrderModel $model */
    private $model;

    /**
     * 构造方法
     * @throws \app\common\exception\BaseException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function initialize()
    {
        parent::initialize();
        $this->model = new OrderModel;
    }

    /**
     * 确认发货
     * @param int $orderId
     * @return array
     * @throws \Exception
     */
    public function delivery(int $orderId)
    {
        // 订单详情
        $model = OrderModel::detail($orderId);
        if ($model->delivery($this->postForm())) {
            return $this->renderSuccess('发货成功');
        }
        return $this->renderError($model->getError() ?: '发货失败');
    }

    /**
     * 修改订单价格
     * @param int $orderId
     * @return array
     */
    public function updatePrice(int $orderId)
    {
        // 订单详情
        $model = OrderModel::detail($orderId);
        if ($model->updatePrice($this->postForm())) {
            return $this->renderSuccess('操作成功');
        }
        return $this->renderError($model->getError() ?: '操作失败');
    }

    /**
     * 审核：用户取消订单
     * @param $orderId
     * @return array|bool
     */
    public function confirmCancel($orderId)
    {
        // 订单详情
        $model = OrderModel::detail($orderId);
        if ($model->confirmCancel($this->postForm())) {
            return $this->renderSuccess('操作成功');
        }
        return $this->renderError($model->getError() ?: '操作失败');
    }

}
