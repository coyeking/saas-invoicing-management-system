<?php
declare (strict_types = 1);

namespace app\store\controller;

use app\store\model\store\User as StoreUserModel;
use app\store\service\store\User as StoreUserService;

/**
 * 商家后台认证
 * Class Passport
 * @package app\store\controller
 */
class Passport extends Controller
{
    /**
     * 强制验证当前访问的控制器方法method
     * 例: [ 'login' => 'POST' ]
     * @var array
     */
    protected $methodRules = [
        'login' => 'POST',
        'logout' => 'POST',
    ];

    /**
     * 商家用户登录
     * @return array|bool|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function login()
    {
        $model = new StoreUserModel;
        if (($userInfo = $model->login($this->postData())) === false) {
            return $this->renderError($model->getError() ?: '登录失败');
        }
        return $this->renderSuccess([
            'userId' => $userInfo['store_user_id'],
            'storeid' => $userInfo['store_id'],
            'token' => $model->getToken()
        ], '登录成功');
    }

    /**
     * 退出登录
     * @return array
     */
    public function logout()
    {
        // 清空登录状态
        StoreUserService::logout();
        return $this->renderSuccess('操作成功');
    }

}
