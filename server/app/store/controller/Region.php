<?php









declare (strict_types = 1);

namespace app\store\controller;

use app\store\model\Region as RegionModel;

/**
 * 地区管理
 * Class Region
 * @package app\store\controller
 */
class Region extends Controller
{
    /**
     * 获取所有地区
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function all()
    {
        $list = RegionModel::getCacheAll();
        return $this->renderSuccess(compact('list'));
    }

    /**
     * 获取所有地区(树状)
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function tree()
    {
        $list = RegionModel::getCacheTree();
        return $this->renderSuccess(compact('list'));
    }

}
