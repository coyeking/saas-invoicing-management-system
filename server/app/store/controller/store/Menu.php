<?php









declare (strict_types = 1);

namespace app\store\controller\store;

use app\store\controller\Controller;
use app\store\model\store\Menu as MenuModel;

/**
 * 商家后台菜单管理
 * Class Menu
 * @package app\store\controller\store
 */
class Menu extends Controller
{
    /**
     * 菜单列表
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function list()
    {
        $model = new MenuModel;
        $list = $model->getList();
        return $this->renderSuccess(compact('list'));
    }
}
