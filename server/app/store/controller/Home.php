<?php









declare (strict_types = 1);

namespace app\store\controller;

use app\store\service\Home as HomeService;

/**
 * 后台首页
 * Class Home
 * @package app\store\controller
 */
class Home extends Controller
{
    /**
     * 后台首页
     * @return array
     */
    public function data()
    {
        // 获取首页数据
        $model = new HomeService;
        $data =  $model->getData();
        return $this->renderSuccess(compact('data'));
    }

}
