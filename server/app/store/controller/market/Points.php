<?php









declare (strict_types = 1);

namespace app\store\controller\market;

use app\store\controller\Controller;
use app\store\model\user\PointsLog as PointsLogModel;

/**
 * 积分管理
 * Class Points
 * @package app\store\controller\market
 */
class Points extends Controller
{
    /**
     * 积分明细
     * @return mixed
     */
    public function log()
    {
        // 积分明细列表
        $model = new PointsLogModel;
        $list = $model->getList($this->request->param());
        return $this->renderSuccess(compact('list'));
    }

}
