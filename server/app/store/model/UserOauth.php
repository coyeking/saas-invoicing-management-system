<?php









declare (strict_types = 1);

namespace app\store\model;

use app\common\model\UserOauth as UserOauthModel;

/**
 * 模型类：第三方用户信息
 * Class UserOauth
 * @package app\store\model
 */
class UserOauth extends UserOauthModel
{

}