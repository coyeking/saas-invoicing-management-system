<?php









declare (strict_types = 1);

namespace app\store\model;

use app\common\model\OrderAddress as OrderAddressModel;

/**
 * 订单收货地址模型
 * Class OrderAddress
 * @package app\store\model
 */
class OrderAddress extends OrderAddressModel
{

}
