<?php









declare (strict_types = 1);

namespace app\store\model;

use app\common\model\Help as HelpModel;

/**
 * 模型类：帮助中心
 * Class Help
 * @package app\store\model
 */
class Help extends HelpModel
{
    /**
     * 新增记录
     * @param array $data
     * @return false|int
     */
    public function add(array $data)
    {
        $data['store_id'] = self::$storeId;
        return $this->save($data);
    }

    /**
     * 更新记录
     * @param array $data
     * @return bool|int
     */
    public function edit(array $data)
    {
        return $this->save($data) !== false;
    }

    /**
     * 删除记录
     * @return bool
     */
    public function setDelete()
    {
        return $this->save(['is_delete' => 1]);
    }

}
