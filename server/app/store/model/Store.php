<?php









declare (strict_types = 1);

namespace app\store\model;

use app\common\model\Store as StoreModel;

/**
 * 商家记录表模型
 * Class Store
 * @package app\store\model
 */
class Store extends StoreModel
{
    /**
     * 更新记录
     * @param array $data
     * @return bool
     */
    public function edit(array $data)
    {
        // 是否删除图片
        !isset($data['logo_image_id']) && $data['logo_image_id'] = 0;
        return $this->save($data) !== false;
    }

}
