<?php









declare (strict_types = 1);

namespace app\store\model\store;

use app\common\model\store\MenuApi as MenuApiModel;

/**
 * 商家后台用户角色与菜单权限关系表模型
 * Class MenuApi
 * @package app\store\model\store
 */
class MenuApi extends MenuApiModel
{
    /**
     * 获取指定角色的所有菜单id
     * @param array $menuIds 菜单ID集
     * @return array
     */
    public static function getApiIds(array $menuIds)
    {
        return (new self)->where('menu_id', 'in', $menuIds)->column('api_id');
    }


}
