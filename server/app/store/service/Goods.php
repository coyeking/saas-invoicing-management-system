<?php









declare (strict_types = 1);

namespace app\store\service;

use app\common\service\Goods as GoodsService;
use app\store\service\goods\Apply as GoodsApplyService;

/**
 * 商品服务类
 * Class Goods
 * @package app\store\service
 */
class Goods extends GoodsService
{
    /**
     * 验证商品是否允许删除
     * @param $goodsId
     * @return bool
     */
    public static function checkIsAllowDelete($goodsId)
    {
        return GoodsApplyService::checkIsAllowDelete($goodsId);
    }

    /**
     * 商品规格是否允许编辑
     * @param int $goodsId
     * @return bool
     */
    public static function checkSpecLocked(int $goodsId)
    {
        return GoodsApplyService::checkSpecLocked($goodsId);
    }

}
