<?php









declare (strict_types = 1);

namespace app\store\service\statistics\data;

use app\store\model\User as UserModel;
use app\common\service\BaseService;

/**
 * 数据统计-用户消费榜
 * Class UserExpendRanking
 * @package app\store\service\statistics\data
 */
class UserExpendRanking extends BaseService
{
    /**
     * 用户消费榜
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getUserExpendRanking()
    {
        return (new UserModel)->field(['user_id', 'nick_name', 'expend_money'])
            ->where('is_delete', '=', 0)
            ->order(['expend_money' => 'DESC'])
            ->limit(10)
            ->select();
    }

}