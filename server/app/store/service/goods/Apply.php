<?php









declare (strict_types=1);

namespace app\store\service\goods;

use app\common\service\Goods as GoodsService;

class Apply extends GoodsService
{
    /**
     * 验证商品规格属性是否锁定
     * @param int $goodsId
     * @return bool
     */
    public static function checkSpecLocked(int $goodsId)
    {
        // 这里实现业务判断
        return false;
    }

    /**
     * 验证商品是否允许删除
     * @param int $goodsId
     * @return bool
     */
    public static function checkIsAllowDelete(int $goodsId)
    {
        // 这里实现业务判断
        return true;
    }

}
