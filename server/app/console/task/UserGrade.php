<?php









declare (strict_types = 1);

namespace app\console\task;

use app\console\service\UserGrade as UserGradeService;

/**
 * 定时任务：会员等级
 * Class UserGrade
 * @package app\console\task
 */
class UserGrade extends Task
{
    // 当前任务唯一标识
    private $taskKey = 'UserGrade';

    // 任务执行间隔时长 (单位:秒)
    protected $taskExpire = 60 * 30;

    // 当前商城ID
    private $storeId;

    /**
     * 任务处理
     * @param array $param
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function handle(array $param)
    {
        ['storeId' => $this->storeId] = $param;
        $this->setInterval($this->storeId, $this->taskKey, $this->taskExpire, function () {
            echo $this->taskKey . PHP_EOL;
            // 设置用户的会员等级
            $this->setUserGrade();
        });
    }

    /**
     * 设置用户的会员等级
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    private function setUserGrade()
    {
        $service = new UserGradeService;
        $service->setUserGrade($this->storeId);
    }

}