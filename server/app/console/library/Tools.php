<?php









declare (strict_types = 1);

namespace app\console\library;

/**
 * 工具类
 * Class Tools
 * @package app\console\library
 */
class Tools
{
    /**
     * 为定时任务写日志
     * @param string $taskKey
     * @param string $method
     * @param array $param
     */
    static function taskLogs(string $taskKey, string $method, array $param = [])
    {
        return log_record(['name' => '定时任务', 'Task-Key' => $taskKey, 'method' => $method, 'param' => $param]);
    }

}