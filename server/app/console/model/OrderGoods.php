<?php









declare (strict_types = 1);

namespace app\console\model;

use app\common\model\OrderGoods as OrderGoodsModel;

/**
 * 订单商品模型
 * Class OrderGoods
 * @package app\console\model
 */
class OrderGoods extends OrderGoodsModel
{

}
