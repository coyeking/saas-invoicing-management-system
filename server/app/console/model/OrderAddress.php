<?php









declare (strict_types = 1);

namespace app\console\model;

use app\common\model\OrderAddress as OrderAddressModel;

/**
 * 订单收货地址模型
 * Class OrderAddress
 * @package app\console\model
 */
class OrderAddress extends OrderAddressModel
{

}
