<?php









declare (strict_types = 1);

namespace app\console\model\recharge;

use app\common\model\recharge\Order as OrderModel;

/**
 * 用户充值订单模型
 * Class Order
 * @package app\console\model\recharge
 */
class Order extends OrderModel
{

}