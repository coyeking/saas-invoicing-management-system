<?php









declare (strict_types = 1);

namespace app\console\model;

use app\common\model\Store as StoreModel;

/**
 * 商家记录表模型
 * Class Store
 * @package app\admin\model
 */
class Store extends StoreModel
{
    /**
     * 获取商城ID集
     * @param bool $isRecycle
     * @return array
     */
    public static function getStoreIds(bool $isRecycle = false)
    {
        $static = new static;
        return $static->where('is_recycle', '=', (int)$isRecycle)
            ->where('is_delete', '=', 0)
            ->order(['create_time' => 'desc', $static->getPk()])
            ->column('store_id');
    }
}