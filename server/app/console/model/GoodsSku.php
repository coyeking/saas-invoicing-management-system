<?php









declare (strict_types = 1);

namespace app\console\model;

use app\common\model\GoodsSku as GoodsSkuModel;

/**
 * 商品规格模型
 * Class GoodsSku
 * @package app\console\model
 */
class GoodsSku extends GoodsSkuModel
{

}
