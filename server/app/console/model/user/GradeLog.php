<?php









declare (strict_types = 1);

namespace app\console\model\user;

use app\common\model\user\GradeLog as GradeLogModel;

/**
 * 用户会员等级变更记录模型
 * Class GradeLog
 * @package app\console\model\user
 */
class GradeLog extends GradeLogModel
{
}