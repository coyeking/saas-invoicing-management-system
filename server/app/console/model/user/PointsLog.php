<?php









declare (strict_types = 1);

namespace app\console\model\user;

use app\common\model\user\PointsLog as PointsLogModel;

/**
 * 用户余额变动明细模型
 * Class PointsLog
 * @package app\console\model\user
 */
class PointsLog extends PointsLogModel
{
}