<?php









declare (strict_types = 1);

namespace app\console\model\user;

use app\common\model\user\BalanceLog as BalanceLogModel;

/**
 * 用户余额变动明细模型
 * Class BalanceLog
 * @package app\console\model\user
 */
class BalanceLog extends BalanceLogModel
{

}