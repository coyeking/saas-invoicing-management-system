<?php









declare (strict_types = 1);

namespace app\console\model;

use app\common\model\OrderRefund as OrderRefundModel;

/**
 * 售后单模型
 * Class OrderRefund
 * @package app\console\model
 */
class OrderRefund extends OrderRefundModel
{

}