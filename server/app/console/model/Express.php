<?php









declare (strict_types = 1);

namespace app\console\model;

use app\common\model\Express as ExpressModel;

/**
 * 物流公司模型
 * Class Express
 * @package app\console\model
 */
class Express extends ExpressModel
{
}