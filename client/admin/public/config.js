window.serverConfig = {
  // 系统名称
  APP_NAME: 'SAAS系统', 
  // 必填: api地址, 换成自己的域名即可
  // 例如: https://www.你的域名.com/index.php?s=/admin
  // BASE_API: 'http://yoshop2.cn/index.php?s=/admin',
  // 必填: store模块的入口地址
  // 例如: https://www.你的域名.com/store
  // STORE_URL: 'http://yoshop2.cn/store',
  
  
  // 本地
  BASE_API: 'http://127.0.0.1:7999/index.php?s=/admin',
  STORE_URL: 'http://127.0.0.1:7999/store',
}
