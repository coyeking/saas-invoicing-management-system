# SAAS进销存管理系统

#### 无数据库，请勿下载


#### 目录
```
├── client                                       // 项目前端
│   ├── admin                                    // 超管后端界面
│   ├── store                                    // 单商户后端界面
│   └── uni-saas-erp                             // uni前端
└── server                                       // 服务端thinkphp
```

#### 介绍
SAAS进销存管理系统


#### 技术特点与软件架构
* 前后端完全分离 (互不依赖 开发效率高)
* 采用PHP7.2 (强类型严格模式)
* Thinkphp6.0.5（轻量级PHP开发框架）
* Uni-APP（开发跨平台应用的前端框架）
* Ant Design Vue（企业级中后台产品UI组件库）
* RBAC（基于角色的权限控制管理）
* 部署运行的项目体积仅30多MB（真正的轻量化）


#### 系统演示
- 暂无

#### 环境要求
- windowns 或 Linux
- Nginx 1.10+
- PHP 7.2+
- MySQL 5.6+


#### 安装教程

1. 将后端源码上传至服务器站点，并且将站点运行目录设置为/public
2. 创建一个数据库，例如：data_db
3. 修改数据库连接文件，将数据库用户名密码等信息填写完整，路径/.env

#### 后台地址

- 超管后台：https://www.你的域名.com/admin
- 商户后台：https://www.你的域名.com/store


#### 页面展示